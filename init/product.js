/**
 * 作者：swl
 * 为数据库中product产品添加数据
 */
const fs = require('fs')
const path = require('path')
const mongoose = require('../db/mongodb')
const product = require('./product.json')
const prodCateModel = require('../models/prodCate')
const productModel = require('../models/prod')

/* 导入数据 */
async function mongoImport() {
  async function fun(Model, list, tip) {
    // 清除数据
    await Model.remove({})
    console.log(`清空集合 ${tip} 成功`)
    // 循环添加数据
    for (let item of list) {
      // 将_id转换为对象类型
      item._id = mongoose.Types.ObjectId(item._id)
      delete item.__v
      await new Model(item).save()
      console.log(`保存 ${tip} 成功`)
    }
  }

  await fun(prodCateModel, product.category, '作品分类')
  await fun(productModel, product.product, '作品')
}

/* 导出数据 */
async function mongoExport() {
  const category = await prodCateModel.find()
  const product = await productModel.find()
  fs.writeFile(path.join(__dirname, 'product.json'), JSON.stringify({ category, product }), 'utf8', () => console.log('作品分组及作品数据导出成功'))
}

// 导出
// mongoExport()
// 导入
mongoImport()
