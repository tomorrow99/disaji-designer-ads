/**
 * 作者：swl
 * product产品数据库模型
 */

const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
  cateId: {
    type: mongoose.Types.ObjectId,  /*产品的分类肯定是来自 prod_cate表 _id*/
    required: [true, '产品分类id不能为空']
  },
  isLock: {
    type: Number
  },
  title: {
    type: String,
    required: [true, '产品标题不能为空'],
    minlength: [2, '产品标题的最小长度为2个字符'],
    maxlength: [30, '产品标题的最大长度为30个字符']
  },
  keywords: {
    type: String,
    required: [true, '产品关键字不能为空'],
    minlength: [2, '产品关键字的最小长度为2个字符'],
    maxlength: [30, '产品关键字的最大长度为30个字符']
  },
  author: {
    type: String,
    required: [true, '作者不能为空'],
    minlength: [2, '作者的最小长度为2个字符'],
    maxlength: [30, '作者的最大长度为30个字符']
  },
  viewCount: {
    type: Number,
    required: [true, '浏览次数不能为空'],
    min: [2, '浏览次数的最小为2次'],
  },
  markup: {
    type: String,
    default: '',
    minlength: [2, '简介的最小长度为2个字符'],
    maxlength: [1000, '简介的最大长度为1000个字符']
  },
  description: {
    type: String,
    required: [true, '产品详情不能为空'],
    minlength: [10, '产品详情的最小长度为10个字符'],
    maxlength: [1000, '产品详情的最大长度为1000个字符']
  },
  cover: {
    type: String
  },
  figure: {
    type: [String]
  }
}, { timestamps: true }); // Schema 的第二个参数是对象，如果传递 timestamps 自动的表里面增加 create_at 和 update_at 字段，代表的记录的添加时间和修改时间，会自行的维护。
const Model = mongoose.model('Product', Schema, 'product');

Model.fields = {
  cateId: '产品的分类',
  isLock: '是否查看',
  title: '产品的标题',
  keywords: '产品的关键字',
  author: '作者',
  viewCount: '浏览次数',
  markup: '产品的简介',
  description: '产品的详情',
  cover: '产品的封面图',
  figure: '详情图'
}

module.exports = Model;
