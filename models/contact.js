/**
* 作者:yh
* 联系我们的信息模型
*/
const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
    tel: {
        type: String,
        required: [true, '电话不能为空'],
    },
    weChat: {
        type: String,
        required: [true, '微信不能为空'],

    },
    qq: String,
    email: String,
    address: String,
});
const Model = mongoose.model('Contact', Schema,'contact');
Model.fields={
    tel:"电话",
    weChat:"微信",
    qq:"QQ",
    email:"邮箱",
    address:"地址",
};

module.exports = Model;


