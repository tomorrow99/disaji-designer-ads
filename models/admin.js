const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
    username: {
        type: String,
    },

    password: {
        type: String,
    },

}, {timestamps: true});

const Model = mongoose.model('Admin', Schema, 'admin');

module.exports = Model;


