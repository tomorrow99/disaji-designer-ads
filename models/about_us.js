/**
* 作者:yzh
* 联系我们的信息模型
*/
const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
    about_s: {
        type: String,
        required: [true, '关于我们的内容不能为空'],
    },
});
const Model = mongoose.model('About_us', Schema,'about_us');
Model.fields={
    about_s:"关于我们",
};

module.exports = Model;


