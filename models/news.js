/**
 * 作者：yy
 * 新闻信息的模型
 */
const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
    title: {
        type: String,
        required: [true, '标题不能为空'],
        minlength: [2, '标题的最小长度为2个字符'],
        maxlength: [100, '分类名称的最大长度为100个字符'],
    },
    description: String,
    islock: String,
    cover: String,
    author: String,
    viewcount: String,
    cateId: mongoose.Types.ObjectId,
}, { timestamps: true })

const Model = mongoose.model('News', Schema, 'news')
Model.fields = {
    title: '新闻标题',
    description: '新闻描述',
    islock: '是否上线',
    cover: '封面图',
    cateId: '分类ID',
    author: '作者',
    viewcount: '浏览次数',
}

module.exports = Model;