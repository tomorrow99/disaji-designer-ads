/**
* 作者:yzh
* 联系我们的信息模型
*/
const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
    server_sort: {
        type: String,
        required: [true, '服务类型不能为空'],
    },
});
const Model = mongoose.model('Server', Schema,'server');
Model.fields={
   server_sort:"服务类型",
};

module.exports = Model;


