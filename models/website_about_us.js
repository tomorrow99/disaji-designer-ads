/**
 * 作者:pp
 * 关于我们的信息模型
 */
const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
    about_us: {
        type: String,
        required: [true, '关于我们的内容不能为空'],
    },
});
const Model = mongoose.model('W_About_us', Schema, 'w_about_us');
Model.fields = {
    about_us: "关于我们",
};

module.exports = Model;