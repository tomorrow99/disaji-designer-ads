/**
 * 作者：swl
 * 产品分类的模型
 */
const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
  cateName: {
    type: String,
    required: [true, '分类名称不能为空'],
    minlength: [2, '分类名称的最小长度为10个字符'],
    maxlength: [30, '分类名称的最大长度为30个字符'],
  }
})

const Model = mongoose.model('ProdCate', Schema, 'prod_cate')
Model.fields = {
  cateName: '分类名称'
}

module.exports = Model;


