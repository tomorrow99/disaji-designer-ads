const mongoose = require('../db/mongodb.js');

/**
 * author：瞿倩婷
 * 功能：登录的数据模型
 */
const logincountSchema = mongoose.Schema({
    username: {
        type: String,
    },

    password: {
        type: String,
    },

}, { timestamps: true });

const loginModel = mongoose.model('AdminCount', logincountSchema, 'adminCount');

module.exports = loginModel;