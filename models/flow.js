/**
* 作者:yzh
* 联系我们的信息模型
*/
const mongoose = require('../db/mongodb.js');

const Schema = mongoose.Schema({
    flow_sort: {
        type: String,
        required: [true, '流程类型不能为空'],
    },
});
const Model = mongoose.model('Flow', Schema,'flow');
Model.fields={
    flow_sort:"流程类型",
};

module.exports = Model;


