function Banner(Banner,Effect,AutoPlay,Callbacks){
    var Bd = jQuery('.bd',Banner),
        Hd = jQuery('.hd',Banner),
        Bar = jQuery('.bar',Bd),
        Item = jQuery('.item',Bar),
        Prev = jQuery('.prev',Banner),
        Next = jQuery('.next',Banner),
        Class = 'on',
        Index = 0,
        _X = DownTime = MoveDistance = Left = 0,
        DMax = -(Item.width()),
        Time,Width,MouseInterval,ImgMove;

    Slide(Index);

    if(!Banner.length || Item.length < 2 || !Effect){
        return;
    }

    switch(Effect){
        case 'Fold':
            Banner.addClass('fold');
            break;
        case 'Slide':
            Banner.addClass('slide');
            break;
    }

    Item.each(function(i,e){
        DMax += jQuery(e).width();
        (i==Index) && jQuery(e).addClass(Class);
        Hd.append('<a href="javascript:;" class="'+(i==Index?Class:'')+'"></a>');
    });

    Hd.children('a').click(function(){
        Index = jQuery(this).index();
        Slide(Index);
        AutoPlay && clearInterval(Time);
        AutoPlay && Auto();
    });

    Prev.click(function(){
        if(Index > 0) Index--;
        else Index = 0;
        Slide(Index);
        AutoPlay && clearInterval(Time);
        AutoPlay && Auto();
    });

    Next.click(function(){
        if(Index < Item.length - 1) Index++;
        else Index = 0;
        Slide(Index);
        AutoPlay && clearInterval(Time);
        AutoPlay && Auto();
    });

    //touch是在移动端使用的
    Banner.on('touchstart',function(e){
        _X = e.originalEvent.targetTouches[0].pageX;	//记录鼠标点下时的位置	!!这条是手机上使用的!!
        ImgMove = true;
        AutoPlay && clearInterval(Time);
        (Effect == 'Slide') && Banner.removeClass('slide');
        Left = parseFloat(Bar.css('left'));
        DownTime = 0;       //记录鼠标点下的到松开的时间
        MouseInterval = setInterval(function(){DownTime++;},1);
    });
    jQuery('body').on('touchmove',function(e){
        if(ImgMove){
            var X = e.originalEvent.targetTouches[0].pageX-_X;	//记录鼠标移动了多远

            MoveDistance = Left+X;
            if(MoveDistance < 0 && MoveDistance > -DMax){
                Bar.css({left:MoveDistance+'px'});
            }
        }
    });
    jQuery('body').on('touchend',function(e){
        if(ImgMove){
            var L = parseFloat(Bar.css('left')),
                W = jQuery(window).width();
            clearInterval(MouseInterval);

            if(DownTime < 50){      //鼠标快速滑过
                if(e.originalEvent.changedTouches[0].pageX > _X && Index > 0){	//向左滑
                    Index--;
                }else if(e.originalEvent.changedTouches[0].pageX < _X && Index < (Item.length-1)){	//向右滑
                    Index++;
                }
            }else{
                Index = Math.abs(Math.round(L/W));
            }

            Slide(Index);
            (Effect == 'Slide') && Banner.addClass('slide');
            AutoPlay && Auto();
        }
        ImgMove = false;
    });

    function Auto(){
        Time = setInterval(function(){
            if(Index < Item.length - 1) Index++;
            else Index = 0;
            Slide(Index);
        },12000);
    }

    function Slide(i){
        Width = jQuery(window).width();
        Bar.css({'left':-(Width*i)+'px'});
        Item.eq(i).addClass(Class).siblings(Item).removeClass(Class);
        Hd.children('a').eq(i).addClass(Class).siblings('a').removeClass(Class);
        jQuery.isFunction(Callbacks) && Callbacks(i,Item.eq(i));
    }

    AutoPlay && Auto();
}

jQuery(window).ready(function(){
    Banner(jQuery('#banner'),'Slide',true,function(i,obj){
        var Header = jQuery('#header'),
            Banner = jQuery('#banner'),
            DataClass = Header.attr('data-class'),
            DataColor = Banner.attr('data-color'),
            Color = obj.attr('data-color')?obj.attr('data-color'):'white';
        Header.hasClass(DataClass) ? Header.removeClass(DataClass) : '';
        Header.addClass(Color).attr('data-class',Color);
        Banner.hasClass(DataColor) ? Banner.removeClass(DataColor) : '';
        Banner.addClass(Color).attr('data-color',Color);
    });
    Banner(jQuery('.banner'),'Slide',true);
});