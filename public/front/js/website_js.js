// JavaScript Document
// oldwebsite
function slideHoliday(){
	setTimeout(function(){
		$('#notice, #notice-mask').fadeOut(500);
	}, 10000);
	$(document).click(function(){$('#notice, #notice-mask').fadeOut(500);})
}

function icoHover(){
	var weibo = $("#Weibo");
	var wechat = $("#Wechat");
	
	weibo.hover(
		function(){
			$(this).find("img").attr("src", "/images/weibo_hover.jpg");
		},
		function(){
			$(this).find("img").attr("src", "/images/weibo.jpg");
		}
	)
	
	wechat.hover(
		function(){
			$(this).children("img").attr("src", "/images/wechat_hover.jpg");
			$(this).children(".qcode").show();
		},
		function(){
			$(this).children("img").attr("src", "/images/wechat.jpg");
			$(this).children(".qcode").hide();
		}
	)
}

function changeBanner(){
	var banner = $("#index_banner").find("li");
	var index = 0;
	var leftIcon = $("#indexLeft");
	var rightIcon = $("#indexRight");
	var auto = null;
	
	banner.eq(0).show();
	auto = 	setInterval(function(){
				if(index == banner.length-1){
					index = 0;
				}else{
					index++;	
				}
				banner.eq(index).fadeIn(500).siblings().fadeOut(800);
			}, 4000);
	
	leftIcon.click(function(){
		clearInterval(auto);
		if(index == banner.length-1){
			index = 0;
		}else{
			index++;	
		}
		banner.eq(index).fadeIn(500).siblings().fadeOut(800);	
		
		auto = 	setInterval(function(){
					if(index == banner.length-1){
						index = 0;
					}else{
						index++;	
					}
					banner.eq(index).fadeIn(500).siblings().fadeOut(800);
				}, 4000);	
	})
	
	rightIcon.click(function(){
		clearInterval(auto);
		if(index == 0){
			index = banner.length-1;
		}else{
			index--;	
		}
		banner.eq(index).fadeIn(500).siblings().fadeOut(800);
		
		auto = 	setInterval(function(){
					if(index == banner.length-1){
						index = 0;
					}else{
						index++;	
					}
					banner.eq(index).fadeIn(500).siblings().fadeOut(800);
				}, 4000);		
	})
}

function setCaseWrapHeight(){
	$("#casecCateWrap").height($("#casecCateWrap").children("ul").height());	
}

function indexNewsTurnPage(){
	var news_list = $("#indexNews");
	var news_page = $("#indexNewsIcon").children("dd"); 	
	var index = 0;
	
	news_page.click(function(){
		index = news_page.index(this);
		$(this).addClass("cur").siblings("dd").removeClass("cur");
		news_list.animate({"top":"-"+index*312+"px"}, "fast", "swing");
	})
}

function changeTeam(){
	var list = $("#teamList").children(".team_block");
	var CateId = $("#curCateId").val();
	
	list.click(function(){
		var index = list.index(this);
		
		if(index == 0){
			$(this).find(".team_select").addClass("manage_cur");
			list.eq(1).find(".team_select").removeClass("designer_cur");
			list.eq(2).find(".team_select").removeClass("sales_cur");
			list.eq(3).find(".team_select").removeClass("tech_cur");
			list.eq(4).find(".team_select").removeClass("as_cur");
			$(this).find(".team_item_wrap").slideDown("fast").end()
				   .siblings(".team_block").find(".team_item_wrap").slideUp("fast");
		}else if(index == 1){
			$(this).find(".team_select").addClass("designer_cur");
			list.eq(0).find(".team_select").removeClass("manage_cur");
			list.eq(2).find(".team_select").removeClass("sales_cur");
			list.eq(3).find(".team_select").removeClass("tech_cur");
			list.eq(4).find(".team_select").removeClass("as_cur");
			$(this).find(".team_item_wrap").slideDown("fast").end()
				   .siblings(".team_block").find(".team_item_wrap").slideUp("fast");
		}else if(index == 2){
			$(this).find(".team_select").addClass("sales_cur");
			list.eq(0).find(".team_select").removeClass("manage_cur");
			list.eq(1).find(".team_select").removeClass("designer_cur");
			list.eq(3).find(".team_select").removeClass("tech_cur");
			list.eq(4).find(".team_select").removeClass("as_cur");
			$(this).find(".team_item_wrap").slideDown("fast").end()
				   .siblings(".team_block").find(".team_item_wrap").slideUp("fast");
		}else if(index == 3){
			$(this).find(".team_select").addClass("tech_cur");
			list.eq(0).find(".team_select").removeClass("manage_cur");
			list.eq(1).find(".team_select").removeClass("designer_cur");
			list.eq(2).find(".team_select").removeClass("sales_cur");
			list.eq(4).find(".team_select").removeClass("as_cur");
			$(this).find(".team_item_wrap").slideDown("fast").end()
				   .siblings(".team_block").find(".team_item_wrap").slideUp("fast");
		}else if(index == 4){
			$(this).find(".team_select").addClass("as_cur");
			list.eq(0).find(".team_select").removeClass("manage_cur");
			list.eq(1).find(".team_select").removeClass("designer_cur");
			list.eq(2).find(".team_select").removeClass("sales_cur");
			list.eq(3).find(".team_select").removeClass("tech_cur");
			$(this).find(".team_item_wrap").slideDown("fast").end()
				   .siblings(".team_block").find(".team_item_wrap").slideUp("fast");
		}		
	})
	
	/*list.hover(
		function(){
			
			if(index == 0){
				$(this).find(".team_select").addClass("manage_cur");
				if(!$(this).find(".team_item_wrap").is(":animated")){
					$(this).find(".team_item_wrap").slideDown("fast");
				}
			}else if(index == 1){
				$(this).find(".team_select").addClass("designer_cur");
				if(!$(this).find(".team_item_wrap").is(":animated")){
					$(this).find(".team_item_wrap").slideDown("fast");
				}
			}else if(index == 2){
				$(this).find(".team_select").addClass("sales_cur");
				if(!$(this).find(".team_item_wrap").is(":animated")){
					$(this).find(".team_item_wrap").slideDown("fast");
				}
			}else if(index == 3){
				$(this).find(".team_select").addClass("tech_cur");
				if(!$(this).find(".team_item_wrap").is(":animated")){
					$(this).find(".team_item_wrap").slideDown("fast");
				}
			}else if(index == 4){
				$(this).find(".team_select").addClass("as_cur");
				if(!$(this).find(".team_item_wrap").is(":animated")){
					$(this).find(".team_item_wrap").slideDown("fast");
				}
			}
		},
		function(){
			var index = list.index(this);
			
			if(index == 0){
				if(CateId != 1){
					$(this).find(".team_select").removeClass("manage_cur");
					$(this).find(".team_item_wrap").slideUp("fast");
				}
			}else if(index == 1){
				if(CateId != 10){
					$(this).find(".team_select").removeClass("designer_cur");
					$(this).find(".team_item_wrap").slideUp("fast");
				}
			}else if(index == 2){
				if(CateId != 14){
					$(this).find(".team_select").removeClass("sales_cur");
					$(this).find(".team_item_wrap").slideUp("fast");
				}
			}else if(index == 3){
				if(CateId != 30){
					$(this).find(".team_select").removeClass("tech_cur");
					$(this).find(".team_item_wrap").slideUp("fast");
				}
			}else if(index == 4){
				if(CateId != 31){
					$(this).find(".team_select").removeClass("as_cur");
					$(this).find(".team_item_wrap").slideUp("fast");
				}
			}	
		}
	)*/
}

function showColorImg(){
	var img_wrap = $("#bImg");
	
	img_wrap.hover(
		function(){
			$(this).children(".bimg_co").fadeIn(300);	
		},
		function(){
			$(this).children(".bimg_co").fadeOut(300);	
		}
	)	
}

function moveMenu(listClass, subClass, marginVal, leftIcon, rightIcon, showNum){
	var menuList = $(listClass);
	var width = menuList.children(subClass).width()+marginVal;
	var length = menuList.children(subClass).length;
	var left = $(leftIcon);
	var right = $(rightIcon);
	var index = 0;
	
	menuList.parent().hover(
		function(){
			if(index == length-7){
				left.show();
			}
			if(index == 0){
				right.show();
			}
		},
		function(){
			left.hide();
			right.hide();
		}
	)
	
	left.click(function(){
		if(index > 0){
			index--;
			if(index > 0){
				right.show();	
			}else if(index == 0){
				left.hide();
			}
			menuList.animate({"left":"-"+width*index+"px"}, "fast");	
		}
	});
	
	right.click(function(){
		if(index < length-showNum){
			index++;
			if(index < length-7){
				left.show();	
			}else if(index == length-7){
				right.hide();	
			}
			menuList.animate({"left":"-"+width*index+"px"}, "fast");	
		}
	});
}

function ajaxHistory(){
	var history = $("#historyWrap").children("li");
	
	history.click(function(){
		var $InfoId = $(this).attr("InfoId");
		
		$(this).addClass("cur").siblings("li").removeClass("cur")
		$.post(
			"/inc/ajax_history.php",
			{
				InfoId	:	$InfoId	
			},
			function(data){
				$("#historyContent").html(data);
			}
		)	
	})
}

function scrollBanner(){
	var banner = $("#reviewBanner").children("ul");
	var length = banner.find("li").size();
	var width = banner.find("li").width();
	var leftIcon = $("#reviewLeft");
	var rightIcon = $("#reviewRight");
	var index = 0;
	
	banner.width(width*length);
	setInterval(function(){
		if(index == length-1){
			index = 0;	
		}else{
			index++;	
		}
		banner.animate({"left":"-"+index*width+"px"});
	}, 4000);
	
	leftIcon.click(function(){
		if(index == 0){
			index = length-1;	
		}else{
			index--;	
		}
		banner.animate({"left":"-"+index*width+"px"});
	})
	
	rightIcon.click(function(){
		if(index == length-1){
			index = 0;	
		}else{
			index++;	
		}
		banner.animate({"left":"-"+index*width+"px"});
	})
}

function historyAnimate(index){
	var bar = $("#Bar");
	var item_list = $("#historyWrap").find(".bar_item");
	var item_cont = $("#historyWrap").find(".bar_cont");
	var distance = new Array(21, 121, 221, 321, 421, 521, 621, 721, 821, 921, 1000);
	var json={}
	var speed = 0;
	var num = null;
	var changeIndex = index;
	
	for(var i=0;i<distance.length;i++){
		json[distance[i]]=i;
	}
	
	for(var i=changeIndex;i<distance.length;i++){
		if(i == 0){
			speed = 1000;	
		}else{
			speed = 5000;	
		}
		
		bar.animate({"width":distance[i]+"px"}, speed, "linear", function(){
			num = parseInt(bar.css("width"));
			item_list.eq(json[num]).addClass("cur");
			item_cont.eq(json[num]).fadeIn(500).siblings(".bar_cont").fadeOut(500);
			
			if(num == 1000){
				resetHistory();	
			}
		});
	}
	
	item_list.click(function(){
		changeIndex = item_list.index(this);
		
		bar.stop(true, false);
		bar.width($(this).css("left"));
		item_cont.eq(changeIndex).fadeIn(500).siblings(".bar_cont").fadeOut(500);
		$("#historyWrap").find(".bar_item:gt("+changeIndex+")").removeClass("cur");
		changeIndex+=1
		$("#historyWrap").find(".bar_item:lt("+changeIndex+")").addClass("cur");
		historyAnimate(changeIndex);
	})
}

function resetHistory(){
	var bar = $("#Bar");
	var item_list = $("#historyWrap").find(".bar_item");	
	var item_cont = $("#historyWrap").find(".bar_cont");

	item_list.removeClass("cur");
	item_cont.fadeOut(500);
	bar.width(0);
	historyAnimate(0);
}

function showContact(){
	var aDiv = $("#bgInfo").children(".js_div");
	var start = null;
	var topAry = new Array(314, 344, 354);
	var index = 0;
	
	setInterval(function(){	
		aDiv.eq(index).animate({"top":topAry[index]+"px", "opacity":"1"}, 400);
		index++;
	}, 500);
}

function showVideo(){
	var imgIco1 = $("#videoShow1");
	var imgIco2 = $("#videoShow2");
	var closeIco1 = $("#closeIco1");
	var closeIco2 = $("#closeIco2");	
	
	imgIco1.click(function(){
		$("#wrap").show();
		$("#wrap").children(".wrap_video").eq(0).show().siblings(".wrap_video").hide();		
	})
	
	imgIco2.click(function(){
		$("#wrap").show();
		$("#wrap").children(".wrap_video").eq(1).show().siblings(".wrap_video").hide();		
	})
	
	closeIco1.click(function(){
		$("#wrap").hide();	
	})
	
	closeIco2.click(function(){
		$("#wrap").hide();	
	})
}

function ajaxGetCase(){
	var statue  = true;
	var page = 1;
	var limitPage = $("#hiddenTotalPage").val();
	var iBtnTop = $("#toTop");
	var iBtnBottom = $("#toBottom");

	$(window).scroll(function(){
		if(page < limitPage){
			
			var scrollHeight	= 	$(document).height()-$(window).height(); //获取滚动条的高度 = 页面总高度 - 可视区域高度
			var scrollTop		= 	$(window).scrollTop();
			var totalDiv 		= 	$("#caseWrap").find(".case_list").size();
			var CateId   		= 	$("#hiddenCateId").val();
			
			if(scrollTop >= scrollHeight-300){
				
				if (!statue){
					return ;
				}
				
				statue = false;
				page++;
				$("#caseWrap").append('<div class="load"></div><div class="blank20"></div>');
				
				setTimeout(function(){
					if(totalDiv >= 15){
						$.ajax({
								type		: 	"GET",
								url			: 	"/inc/get_case.php",
								data		: 	"page="+page+"&CateId="+CateId,
								success		:	function(data){
												if(data){
													$("#caseWrap .blank35").remove();
													$("#caseWrap .load").remove();
													$("#caseWrap .blank20").remove();
													$("#caseWrap").append(data);
													$("#caseWrap .case_list").fadeIn("slow");
													$("#caseWrap").append('<div class="blank35"></div>');
													statue = true;							
												}
							   }
						});
					}
				}, 1000)
			}
		}
	});	
	
	$(window).scroll(function(){
		if($(window).scrollTop() > 300){
			iBtnTop.fadeIn(200);
			iBtnBottom.fadeIn(200);	
		}else{
			iBtnTop.hide();
			iBtnBottom.hide();	
		}
	})
	
	iBtnTop.mousedown(function(){
		$("body, html").animate({"scrollTop":0}, "slow");	
	})
	
	iBtnTop.mouseup(function(){
		$("body, html").stop();		
	})
	
	iBtnBottom.mousedown(function(){
		var height = $(document).height();
		$("body, html").animate({"scrollTop":height}, "slow");	
	})
	
	iBtnBottom.mouseup(function(){
		$("body, html").stop();		
	})
}

function popService(data){
	var num = parseInt(Math.random()*data.length);
	var service = data[num];
	var contact = $('#pop_service').find('.contact');
	var later = $('#pop_service').find('.later');
	var cls = $('#pop_service').find('.cls');
		
	contact.text('联系顾问 '+service.QQName).attr('href', 'http://wpa.qq.com/msgrd?v=3&uin='+service.QQNumber+'&site=qq&menu=yes');
	setTimeout(function(){
		$('#pop_service').show();
		$('input[name=count]').val(1)
	}, 25000);
	
	later.add(cls).click(function(){
		var count = $('input[name=count]').val();
		var num = parseInt(Math.random()*data.length);	
		var service = data[num];
		
		$('#pop_service').hide();
		if($('input[name=count]').val() == 1){
			$('input[name=count]').val(2);
			contact.text('联系顾问 '+service.QQName).attr('href', 'http://wpa.qq.com/msgrd?v=3&uin='+service.QQNumber+'&site=qq&menu=yes');
			setTimeout(function(){
				$('#pop_service').show();	
			}, 60000)
		}else if($('input[name=count]').val() == 2){
			$('input[name=count]').val(3);
			contact.text('联系顾问 '+service.QQName).attr('href', 'http://wpa.qq.com/msgrd?v=3&uin='+service.QQNumber+'&site=qq&menu=yes');
			setTimeout(function(){
				$('#pop_service').show();	
			}, 90000)
		}
	})
}