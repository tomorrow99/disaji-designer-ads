// 数据库名为disaji
const mgDsn = process.env.NODE_ENV == 'development' ?
    'mongodb://localhost:27017/disaji' :
    'mongodb://120.24.68.240:27017/disaji'

module.exports = {
    mgDsn
}