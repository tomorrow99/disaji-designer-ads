# 初始化仓库
  - 下载npm包： npm install
  - 初始化数据库：npm run init
  - 运行项目： npm run dev
  - 前台：http://localhost:3000/index
  - 后台：http://localhost:3000/admin/index

# 目录说明
  - config 数据库配置
  - controllers 具体业务
  - db 数据库连接实例
  - models 数据库模型
  - public 静态文件
  - routes 路由
  - templateEngine 模板引擎配置
  - views 后台html模板
  - views/front 前台html模板
  - init 初始化数据库

# 文件说明
  - controllers/backController.js 退出登录的控制器
  - models/admin.js 管理员的数据库模型
  - routes/
    + back.js 退出登录的路由
    + common.js 管理员登录的路由（未接入数据库，任意用户可登录）
    + front.js 前台页面的路由
    + index.js 后台路由

# 使用技术
  - express框架、mongodb数据库、art-template模板引擎

# 前台页面链接
  - 标志设计：http://localhost:3000/prodlist/list-5.html
  - 品牌设计：http://localhost:3000/prodlist/list-8.html
  - 画册包装：http://localhost:3000/prodlist/list-9.html
