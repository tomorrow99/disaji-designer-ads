const path = require('path');

module.exports = (app) => {
  app.engine('html', require('express-art-template'));
  app.set('view options', {
    debug: process.env.NODE_ENV !== 'production'
  });

  /* 注意：在使用的模板引擎目录的时候，需要设置为当前目录的上级目录 */
  // console.log(path.join(__dirname, '../views'));

  app.set('views', path.join(__dirname, '../views'));
  app.set('view engine', 'html');
}
