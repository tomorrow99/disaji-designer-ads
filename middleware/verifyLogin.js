/*使用这个中间件函数去验证用户是否登录*/
function verifyLogin(req, res, next) {
  if (req.cookies && req.cookies['isLogin'] != 1) {
    res.redirect('/admin/login');
  } else {
    next();
  }
}

module.exports = {
  verifyLogin,
}
