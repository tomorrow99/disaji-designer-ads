const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

const verifyLoginMiddleware = require('./middleware/verifyLogin.js');
const artTemplate = require('./templateEngine/artTemplate.js');

const indexRouter = require('./routes/index.js');
const backRouter = require('./routes/back.js');
const frontRouter = require('./routes/front.js');

/*和后台相关，但是不需要登录的操作*/
const commonRouter = require('./routes/common.js');

/* swl的作品展示后台路由 begin */
const productRouter = require('./routes/product.js')
    /* swl的作品展示后台路由 end */

/* yh的作品展示后台路由 begin */
const contactRouter = require('./routes/contact.js')
    /* yh的作品展示后台路由 end */
    /* yy的新闻相关操作后台路由 begin */
const newsRouter = require('./routes/news.js')
    /* yy的新闻相关操作后台路由 end */

/* yzh的作品展示后台路由 begin */
const footerRouter = require('./routes/footer.js')
    /* yzh的作品展示后台路由 begin */

/* yzh的作品展示后台路由 begin */
const footer1Router = require('./routes/footer1.js')
    /* yzh的作品展示后台路由 begin */

/* yzh的作品展示后台路由 begin */
const footer2Router = require('./routes/footer2.js')
    /* yzh的作品展示后台路由 begin */


/* pp的作品展示后台路由 begin */
const websiteRouter = require('./routes/website.js')
    /* pp的作品展示后台路由 begin */

const app = express();
const port = 3000;

/* cookie 处理 */
app.use(cookieParser())

/*定义art-template模板引擎*/
artTemplate(app);

// bodyParser post解析
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

/*定义静态资源目录*/
app.use(express.static('public'))

/*下面的路由是不要做登录检测*/
app.use('/admin', commonRouter);
app.use(frontRouter);
// 登录检测的
let verifyFn = verifyLoginMiddleware.verifyLogin;
app.use('/admin', verifyFn, indexRouter);
app.use('/admin', verifyFn, backRouter);

/* swl的作品展示后台路由 begin */
app.use('/admin', verifyFn, productRouter);
/* swl的作品展示后台路由 end */

/* yh的作品展示后台路由 begin */
app.use('/admin/contact', contactRouter);
/* yh的作品展示后台路由 begin */
/* yy的新闻展示后台路由 begin */
app.use('/admin/news', verifyFn, newsRouter);
/* yy的新闻展示后台路由 begin */

/* yzh的作品展示后台路由 begin */
app.use('/admin/server', footerRouter);
/* yzh的作品展示后台路由 begin */

/* yzh的作品展示后台路由 begin */
app.use('/admin/flow', footer1Router);
/* yzh的作品展示后台路由 begin */

/* yzh的作品展示后台路由 begin */
app.use('/admin/about_us', footer2Router);
/* yzh的作品展示后台路由 begin */

/* pp的作品展示后台路由 begin */
app.use('/admin/website', websiteRouter);
/* pp的作品展示后台路由 begin */

app.listen(port, () => {
    console.log(`project listening at http://localhost:${port}`)
})