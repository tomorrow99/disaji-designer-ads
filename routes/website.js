/**
 * 作者:pp
 * 路由规则
 */
const express = require('express');
const router = express.Router(); // 路由器

const websiteController = require('../controllers/websiteController.js');

router.get('/about_us-show', websiteController.show);
router.post('/about_us-add', websiteController.add);
router.get('/about_us-del', websiteController.del);

module.exports = router;