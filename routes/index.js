const express = require('express');
const IndexController = require('../controllers/indexController')
const router = express.Router(); // 路由器

/*单独的路由文件，一般称之为：路由外置；路由外置文件里面的 路由url地址：app.use('/admin', indexRouter); + /index*/
router.get('/index', IndexController.InnerAdminIndex);
router.get('/welcome', IndexController.Welcome);
/* 暴露出去以后，谁需要这个路由信息，导入即可*/
module.exports = router;