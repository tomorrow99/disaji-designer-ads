/**
* 作者:yzh
* 路由规则
*/
const express = require('express');
const router = express.Router(); // 路由器
const footerController = require('../controllers/footerController.js')
// const ServerController = require('../controllers/footerController.js');

// const FlowController = require('../controllers/footerController.js');

    router.post('/add', footerController.ServerController.addServer);
    router.get('/server_add', footerController.ServerController.server_add);
    router.get('/server-list', footerController.ServerController.showServer);
    router.get('/delete', footerController.ServerController.delServer);
    router.get('/edtserver/:id', footerController.ServerController.editServer);
    router.post('/edit', footerController.ServerController.edit);




    


module.exports = router