const express = require('express');
const router = express.Router(); // 路由器
// const loginModel = require('../models/loginAdmin')
const BackController = require('../controllers/backController.js');
// const md5 = require('md5')
/*后台的退出  */
router.get('/logout', BackController.logout)
module.exports = router;