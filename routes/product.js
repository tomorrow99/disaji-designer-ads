/**
 * 作者：swl
 * TODO:product产品相关路由规则
 */

const express = require('express')
const router = express.Router()

const ProductController = require('../controllers/productController')
const multer = require('multer')
const path = require('path')
// 文件上传
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../public/uploads/'))
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
  }
})
const upload = multer({ storage })

// 渲染添加产品分类页面
router.get('/prodcate/add', (req, res) => {
  res.render('product/prod-cate-add', { title: '产品分类添加', info: {} })
})
// 渲染产品分类页面
router.get('/prodcate/list', ProductController.categoryList)
// 渲染分类的编辑页
router.get('/prodcate/edit/:id', (req, res) => {
  res.render('product/prod-cate-edt', { title: '产品分类修改', info: {_id: req.params.id} })
})
// 渲染添加产品页面
router.get('/product/add', ProductController.productAdd)
// 渲染产品的编辑页
router.get('/product/edit/:id', ProductController.prodEdit)
// 渲染产品列表页面
router.get('/product/list', ProductController.productList)

/* 后台操作数据的路由 */
// 添加产品分类
router.post('/prodcate/store', ProductController.cateStore)
// 删除产品分类
router.get('/prodcate/delete/:id', ProductController.cateDelete)
// 修改产品分类
router.post('/prodcate/edit', ProductController.cateEdit)
// 添加产品
router.post('/product/store', upload.fields([{ name: 'cover' }, { name: 'figure' }]), ProductController.productStore)
// 删除产品
router.get('/product/delete/:id', ProductController.productDelete)
// 修改产品
router.post('/product/edit', upload.fields([{ name: 'cover' }, { name: 'figure' }]), ProductController.productEdit)
// 产品详情
router.get('/product/detail/:id', ProductController.productDetail)
// 控制产品是否显示
router.get('/product/lock/:id', ProductController.productLock)

module.exports = router
