const express = require('express');


const contactModel = require('../models/contact.js');
const serverModel = require('../models/server.js');
const flowModel = require('../models/flow.js');
const about_usModel = require('../models/about_us.js');



/* TODO:swl的前台作品展示路由 begin */
const ProductController = require('../controllers/productController')
    /* swl的前台作品展示路由 end */

/* yh的作品展示后台路由 begin */
const ContactController = require('../controllers/contactController.js');
/* yh的作品展示后台路由 end */
/* yy的新闻展示后台路由 begin */
const NewsController = require('../controllers/newsController');
/* yy的新闻展示后台路由 end */

const websiteController = require('../controllers/websiteController')
const router = express.Router(); // 路由器
const NewsCateModel = require('../models/newsCate');

/**
 * author：瞿倩婷
 * 前端页面路由
 */
// router.get('/index', (req, res) => {
//         res.render('front/index');
//     })
//     /**qqt 前端页面路由end */


/* TODO:yy的前台新闻展示路由 begin */
//首页需要携带数据传递过去，所以在基础上进行了修改
router.get('/', (req, res) => res.redirect('/index'))
router.get('/index', NewsController.Newslist)
    /* yy的前台新闻展示路由 end */

/* TODO:swl的前台作品展示路由 begin */
router.get('/prodlist/:id', ProductController.prodlist)
router.get('/proddetail/:id', ProductController.proddetail)
    /* swl的前台作品展示路由 end */


// ProductController.aboutURI
/*TODO:pp的前台作品展示路由 begin*/
router.get('/about', websiteController.show_front)
    /* pp的前台作品展示路由 end */

/*TODO:yh的前台作品展示路由 begin*/
router.get('/contact', ContactController.about);
/* yh的前台作品展示路由 end */


/* TODO:yy的前台更多新闻展示路由 list-3.html begin */
router.get('/news/more', NewsController.newsMore);
/* yy的前台更多新闻展示路由 end */
/* TODO:yy的前台新闻详情展示路由 list-3.html begin */
router.get('/news/detail/:_id', NewsController.NewsDetail);
/* yy的前台新闻详情展示路由 end */

router.get('/serveOrder', async(req, res) => {
    /* 从数据库里面把网站的基本的信息取出来 */
    const serverinfo = await serverModel.find();
    const flowinfo = await flowModel.find();
    const about_usinfo = await about_usModel.find();
    const info = await contactModel.findOne();
    //console.log(serverinfo);
    //    res.render('front/about-11', { info: info,serverinfo:serverinfo,flowinfo:flowinfo,about_usinfo:about_usinfo});
    res.render('front/about-10.html', { info: info, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo })
})

module.exports = router;