/**
* 作者:yzh
* 路由规则
*/
const express = require('express');
const router = express.Router(); // 路由器
const footerController = require('../controllers/footerController.js')


router.post('/add', footerController.About_usController.addAbout_us);
router.get('/about_us-add', footerController.About_usController.about_us_add);
router.get('/about_us-list', footerController.About_usController.showAbout_us);
router.get('/delete', footerController.About_usController.delAbout_us);
router.get('/edtabout_us/:id', footerController.About_usController.editAbout_us);
router.post('/edit', footerController.About_usController.edit);







module.exports = router