/**
* 作者:yzh
* 路由规则
*/
const express = require('express');
const router = express.Router(); // 路由器
const footerController = require('../controllers/footerController.js')

router.post('/add', footerController.FlowController.addFlow);
router.get('/flow_add', footerController.FlowController.flow_add);
router.get('/flow-list', footerController.FlowController.showFlow);
router.get('/delete', footerController.FlowController.delFlow);
router.get('/edtflow/:id', footerController.FlowController.editFlow);
router.post('/edit', footerController.FlowController.edit);



module.exports = router;