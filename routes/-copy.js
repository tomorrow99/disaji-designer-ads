const express = require('express');
const router = express.Router(); // 路由器

const BackController = require('../controllers/backController.js');

/*后台的退出 /admin/logout */
router.get('/logout', BackController.logout);

module.exports = router;
