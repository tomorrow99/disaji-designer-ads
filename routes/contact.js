/**
* 作者:yh
* 路由规则
*/
const express = require('express');
const router = express.Router(); // 路由器

const ContactController = require('../controllers/contactController.js');

router.get('/add', ContactController.add);
router.post('/store', ContactController.editContact);

module.exports = router;