/**
 * author：瞿倩婷
 * 后台登陆
 */
// 不需要验证的操作
const express = require('express');

const commonController = require('../controllers/commonController')

const router = express.Router(); // 路由器

/* 后台登录 */
router.get('/login', commonController.AdminLogin);

/*登录检测信息是否正确*/
router.post('/loginCheck', commonController.LoginCheck)
module.exports = router;