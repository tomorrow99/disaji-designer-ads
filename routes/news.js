/**
 * 作者：yy
 * TODO:new新闻相关路由规则
 */

const express = require('express');
const router = express.Router();
const CategoryNewsModel = require('../models/newsCate');
const NewsModel = require('../models/news');
const newsController = require('../controllers/newsController');
const moment = require('moment');
const multer = require('multer')
const path = require('path')
const mongoose = require('mongoose');
// 文件上传
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname, '../public/uploads/'))
    },
    filename: function(req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
    }
})
const upload = multer({ storage });

//渲染后台界面
// 渲染添加新闻分类页面
router.get('/category/add', (req, res) => {
    res.render('news/news-cate-add', { title: '新闻分类的添加' });
});
//渲染分类信息展示页面
router.get('/category/list', async(req, res) => {
    const infos = await CategoryNewsModel.find();
    res.render('news/news-cate-list', { title: '分类信息展示', infos });
});
//渲染分类信息编辑界面
router.get('/category/edit/:cateId', async(req, res) => {
    const cateId = req.params.cateId;
    const cateName = req.body;
    const info = await CategoryNewsModel.findById(cateId);
    res.render('news/news-cate-edit', { title: '分类信息编辑', info })

});
// 渲染添加新闻页面
router.get('/add', async(req, res) => {
    const categoryInfo = await CategoryNewsModel.find();
    res.render('news/news-add', { title: '新闻信息的添加', categoryInfo });
});
//渲染新闻信息展示页面
router.get('/list', newsController.getNewsList);
//渲染新闻信息编辑界面
router.get('/edit/:Id', async(req, res) => {
    const Id = req.params.Id;
    const info = await NewsModel.findById(Id);
    const categoryInfo = await CategoryNewsModel.find();
    res.render('news/news-edit', { title: '新闻信息编辑', info, categoryInfo });

});
//渲染新闻后台详情界面
router.get('/detail/:Id', async(req, res) => {
    let newsId = req.params.Id;
    //获取所有的分类信息
    const info = await NewsModel.aggregate([{
        //尝试把字符串_id转换为MongoDB里面ObjectId类型
        $match: { _id: mongoose.Types.ObjectId(newsId) } //$match代表根据条件查询
    }, {
        //这个我们称之为联表操作，我们现在是新闻表(cateId)要和分类表(_id)产生联系
        $lookup: {
            //要关联的表 category
            from: 'newscate',
            //自己表里面和别的表产生关系的id
            localField: 'cateId',
            //关联表的id
            foreignField: '_id',
            //查询到的关联信息的key值
            as: 'cateInfo'
        }
    }]);
    // res.json(info[0]);
    res.render('news/news-detail', { title: '新闻的资源详情', info: info[0], moment });
});

/* 后台操作数据的路由 */
// 添加新闻分类
router.post('/category/store', newsController.addCategory);
// 修改新闻分类
router.post('/category/edit/store', newsController.updateCategory);
//删除新闻分类
router.get('/category/delete/:cateId', newsController.deleteCategory);
module.exports = router;
// 添加新闻
router.post('/store', upload.single('cover'), newsController.addNews);
// 修改新闻
router.post('/edit/store', upload.single('cover'), newsController.updateNews);
//删除新闻
router.get('/delete/:cateId', newsController.deleteNews);
module.exports = router;