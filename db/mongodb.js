/*1. 根据数据库的类型，拆分为不同的数据库操作文件*/
const mongoose = require('mongoose');
const dbConfig = require('../config/db.js');


mongoose.connect(dbConfig.mgDsn, {useNewUrlParser: true, useUnifiedTopology: true});

/* 在模型文件夹下的各个模型需要使用的 mongoose ，需要导出 */
module.exports = mongoose;