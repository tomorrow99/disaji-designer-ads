const CategoryNewsModel = require('../models/newsCate');
const NewsModel = require('../models/news');
const ProductModel = require('../models/prod');

const contactModel = require('../models/contact.js');
const serverModel = require('../models/server.js');
const flowModel = require('../models/flow.js');
const about_usModel = require('../models/about_us.js');


const mongoose = require('mongoose');
const moment = require('moment');
const multer = require('multer')
const path = require('path')
    // 文件上传
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname, '../public/uploads/'))
    },
    filename: function(req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
    }
})
const upload = multer({ storage })

class NewsController {
    /*TODO:其他操作数据 */
    // 渲染新闻列表前台页
    static Newslist = async(req, res) => {
            const cateInfo = await CategoryNewsModel.find();
            const NowId = cateInfo[0]._id;
            const cateId = req.query.cateId ? req.query.cateId : NowId;
            const newsInfo = await NewsModel.aggregate([{
                $match: {
                    cateId: mongoose.Types.ObjectId(cateId),
                }
            }, {
                $match: {
                    islock: '1'
                }
            }, {
                //这个我们称之为联表操作，我们现在是新闻表(cateId)要和分类表(_id)产生联系
                $lookup: {
                    //要关联的表 category
                    from: 'newscate',
                    //自己表里面和别的表产生关系的id
                    localField: 'cateId',
                    //关联表的id
                    foreignField: '_id',
                    //查询到的关联信息的key值
                    as: 'NewsCateInfo'
                }
            }]);
            const serverinfo = await serverModel.find();
            const flowinfo = await flowModel.find();
            const about_usinfo = await about_usModel.find();
            const info = await contactModel.findOne();

            // 首页产品信息
            const products = await ProductModel.find()
            res.render('front/index', { title: '新闻的资源展示', info: info, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo, cateInfo, newsInfo, moment, products });
        }
        //渲染新闻更多前台页面
    static newsMore = async(req, res) => {
            let { page = 1 } = req.query;
            const size = 1;
            page = parseInt(page);

            // let count = await NewsModel.count();
            // console.log('count:' + count);
            let offset = (page - 1) * size;
            const cateInfo = await CategoryNewsModel.find();
            const NowId = cateInfo[0]._id;
            const cateId = req.query.cateId ? req.query.cateId : NowId;
            let NewsCateInfo = await CategoryNewsModel.find({ _id: cateId });
            const total = await NewsModel.aggregate([{
                $match: {
                    cateId: mongoose.Types.ObjectId(cateId),
                },
            }, {
                $match: {
                    islock: '1'
                }
            }, {
                //这个我们称之为联表操作，我们现在是新闻表(cateId)要和分类表(_id)产生联系
                $lookup: {
                    //要关联的表 category
                    from: 'newscate',
                    //自己表里面和别的表产生关系的id
                    localField: 'cateId',
                    //关联表的id
                    foreignField: '_id',
                    //查询到的关联信息的key值
                    as: 'NewsCateInfo'
                }
            }, { $group: { _id: "$cateId", count: { $sum: 1 } } }]);
            const newsInfo = await NewsModel.aggregate([{
                $match: {
                    cateId: mongoose.Types.ObjectId(cateId),
                },
            }, {
                $skip: offset
            }, {
                $limit: size
            }, {
                //这个我们称之为联表操作，我们现在是新闻表(cateId)要和分类表(_id)产生联系
                $lookup: {
                    //要关联的表 category
                    from: 'newscate',
                    //自己表里面和别的表产生关系的id
                    localField: 'cateId',
                    //关联表的id
                    foreignField: '_id',
                    //查询到的关联信息的key值
                    as: 'NewsCateInfo'
                }
            }]);
            let count = 0;
            if (total[0]) {
                count = total[0].count == 'undefined' ? 0 : parseFloat(total[0].count);
            }
            // console.log(total);

            let totalPage = Math.ceil(count / size); //小数11条/2条=5.5向上取整5.3=6

            const serverinfo = await serverModel.find();
            const flowinfo = await flowModel.find();
            const about_usinfo = await about_usModel.find();
            const info = await contactModel.findOne();
            // res.json(NewsCateInfo[0].cateName);
            res.render('front/news-list', { page, size, cateId, count, totalPage, NewsCateInfo, title: '新闻的资源展示', cateInfo, newsInfo, moment, info: info, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo });
        }
        // 渲染新闻详情前台页
    static NewsDetail = async(req, res) => {
            const _id = req.params._id;
            const info = await NewsModel.findOne({ _id: _id }, (error, data) => {
                data.viewcount = +data.viewcount + 1;
                data.save();
            });
            const serverinfo = await serverModel.find();
            const flowinfo = await flowModel.find();
            const about_usinfo = await about_usModel.find();
            const infos = await contactModel.findOne();
            res.render('front/news-detail', { title: '新闻的资源展示', info, moment, infos: infos, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo });
        }
        /* TODO: 后台操作数据 */
        // 添加新闻分类
    static async addCategory(req, res) {
            const { cateName } = req.body;
            const newObj = new CategoryNewsModel({ cateName });
            const error = newObj.validateSync();
            if (error) {
                res.render('error', {
                    errorRs: Object.keys(error.errors).map(attr => `${CategoryNewsModel.fields[attr]} 字段：${error.errors[attr]}`),
                    url: '/admin/news/category/add',
                    time: 3
                })
                return
            }
            try {
                await newObj.save()
                res.redirect('/admin/news/category/list')
            } catch (e) {
                res.redirect('/admin/product/add')
            }
        }
        //修改新闻分类
    static async updateCategory(req, res) {
            let { _id, cateName } = req.body;
            const info = await CategoryNewsModel.findOne({ _id: _id }, async(error, data) => {
                if (error) {
                    res.redirect('back');
                } else {
                    data.cateName = cateName;
                    await data.save();
                    res.redirect('/admin/news/category/list');
                }
            });
        }
        //删除新闻分类
    static async deleteCategory(req, res) {
            const cateId = req.params.cateId;
            const info = await CategoryNewsModel.deleteOne({ _id: cateId });
            res.redirect('/admin/news/category/list');


        }
        //添加新闻
    static async addNews(req, res) {
            let cover = req.file ? req.file.filename : '';
            const { title, author, viewcount, description, islock, cateId } = req.body;
            const newObj = new NewsModel({ title, author, viewcount, description, islock, cover, cateId });
            const error = newObj.validateSync();
            if (error) {
                res.render('error', {
                    errorRs: Object.keys(error.errors).map(attr => `${NewsModel.fields[attr]} 字段：${error.errors[attr]}`),
                    url: '/admin/news/add',
                    time: 3
                })
                return
            }
            try {
                await newObj.save()
                res.redirect('/admin/news/list')
            } catch (e) {
                res.redirect('/admin/news/add')
            }
        }
        //获取新闻列表页进行展示
    static getNewsList = async(req, res) => {
            let { page = 1 } = req.query;
            let size = 2;
            page = parseInt(page);
            // size = parseInt(size);
            let count = await NewsModel.count();
            // res.json(count);
            // console.log('count:' + count);
            let offset = (page - 1) * size;
            let totalPage = Math.ceil(count / size); //小数11条/2条=5.5向上取整5.3=6
            const infos = await NewsModel.aggregate([{
                    $skip: offset
                }, {
                    $limit: size
                },
                {
                    //这个我们称之为联表操作，我们现在是新闻表(cateId)要和分类表(_id)产生联系
                    $lookup: {
                        //要关联的表 category
                        from: 'newscate',
                        //自己表里面和别的表产生关系的id
                        localField: 'cateId',
                        //关联表的id
                        foreignField: '_id',
                        //查询到的关联信息的key值
                        as: 'cateInfo'
                    }
                }
            ]);

            const serverinfo = await serverModel.find();
            const flowinfo = await flowModel.find();
            const about_usinfo = await about_usModel.find();
            const info = await contactModel.findOne();

            res.render('news/news-list', { page, size, count, totalPage, title: '新闻的资源展示', infos, moment, info: info, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo });
        }
        //修改新闻
    static async updateNews(req, res) {
            let { _id, title, author, viewcount, description, cover, islock, cateId } = req.body;
            const info = await NewsModel.findOne({ _id: _id }, async(error, data) => {
                if (error) {
                    res.redirect('back');
                } else {
                    data.title = title;
                    data.author = author;
                    data.viewcount = viewcount;
                    data.description = description;
                    data.islock = islock;
                    if (req.file) {
                        data.cover = req.file.filename;
                    }
                    await data.save();
                    res.redirect('/admin/news/list');
                }
            });
        }
        //删除新闻
    static async deleteNews(req, res) {
        const cateId = req.params.cateId;
        const info = await NewsModel.deleteOne({ _id: cateId });
        res.redirect('/admin/news/list');
    }
}
module.exports = NewsController;