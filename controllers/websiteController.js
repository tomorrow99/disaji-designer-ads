/**
 * 作者:pp
 * 关于我们的业务功能
 */

const w_about_usModel = require('../models/website_about_us.js');

/**
 * 引入联系模块的路由
 */
const contactModel = require('../models/contact.js');
const serverModel = require('../models/server.js');
const flowModel = require('../models/flow.js');
const about_usModel = require('../models/about_us.js');


class WebsiteController {

    static show = async(req, res) => {
        const infos = await w_about_usModel.find()
            // console.log('infos中的信息', infos)
        res.render('website/about_us-show', { infos, info: {} });
    }


    // 给前台传数据
    static show_front = async(req, res) => {
        const infos = await w_about_usModel.find()

        /* 联系模块获取的数据 */
        const serverinfo = await serverModel.find();
        const flowinfo = await flowModel.find();
        const about_usinfo = await about_usModel.find();
        const message = await contactModel.findOne();

        res.render('front/about-1.html', { infos, info: {}, message, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo });
    }




    static del = async(req, res) => {
        const result = await w_about_usModel.remove({})
        console.log('result', result)
        res.render('website/about_us-show', { infos: [], info: {} });
    }

    static add = async(req, res) => {
        let { about_us } = req.body;
        let cateObj = new w_about_usModel({ about_us });
        let error = cateObj.validateSync();
        if (error) {
            const errorRs = [];
            for (let attr in error.errors) {
                errorRs.push(`${w_about_usModel.fields[attr]} 字段：${error.errors[attr]}`);
            }
            let url = '/admin/website/about_us-show';
            let time = 3;
            res.render('error', { errorRs, url, time });
            return;
        }

        // 信息合法
        try {
            let info = await cateObj.save();
            // console.log('info', info)
            res.redirect('/admin/website/about_us-show');
        } catch (e) {
            res.redirect('/admin/website/about_us-show');
        }

    };
}
module.exports = WebsiteController;