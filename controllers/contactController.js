/**
* 作者:yh
* 联系我们的业务功能
*/

const contactModel = require('../models/contact.js');
const serverModel = require('../models/server.js');
const flowModel = require('../models/flow.js');
const about_usModel = require('../models/about_us.js');
class ContactController {

    static editContact = async (req, res) => {

        let { _id, tel, weChat, qq, email, address } = req.body;
        if (_id) {
            // 代表是更新操作，否则为添加操作
            contactModel.findById(_id, (error, data) => {
                /*tel:"电话", weChat:"微信", qq:"QQ", email:"邮箱", address:"地址"*/
                if (error) {
                    res.send('update failure!');
                    return;
                } else {
                    let error = data.validateSync();
                    if (error) {
                        const errorRs = [];
                        for (let attr in error.errors) {
                            errorRs.push(`${contactModel.fields[attr]}字段,${error.errors[attr]}`);
                        }
                        let url = '/admin/contact/add';
                        let time = 3;
                        res.render('error', { errorRs, url, time });
                    } else {
                        data.tel = tel;
                        data.weChat = weChat;
                        data.qq = qq;
                        data.email = email;
                        data.address = address;
                        data.save((error, data) => {
                            if (error) {
                                console.log(error);
                                res.redirect('/admin/contact/add');
                            } else {
                                console.log(data);
                                res.redirect('/admin/contact/add');
                            }
                        });
                    }
                }
            })

        } else {
            let contactObj = new contactModel({
                tel,
                weChat,
                qq,
                email,
                address
            });
            var error = contactObj.validateSync();
            if (error) {
                const errorRs = [];
                for (let attr in error.errors) {
                    errorRs.push(`${contactModel.fields[attr]}字段,${error.errors[attr]}`);
                }
                let url = '/admin/contact/add';
                let time = 5;
                res.render('error', { errorRs, url, time });
            } else {
                contactObj.save((error, data) => {
                    if (error) {
                        console.log(error);
                        res.redirect('/admin/contact/add');
                    } else {
                        console.log(data);
                        res.redirect('/admin/contact/add');
                    }
                });
            }
        }
    }
    static add = async (req, res) => {
        const info = await contactModel.findOne();
        if (info) {
            res.render('contact-add', { info }); // {}

        } else {
            res.render('contact-add', { info: {} });
        }
    }
    static about = async (req, res) => {
        /* 从数据库里面把网站的基本的信息取出来 */
        const serverinfo = await serverModel.find();
        const flowinfo = await flowModel.find();
        const about_usinfo = await about_usModel.find();
        const info = await contactModel.findOne();
        //console.log(serverinfo);
        res.render('front/about-11', { info: info,serverinfo:serverinfo,flowinfo:flowinfo,about_usinfo:about_usinfo});
    }
}
module.exports = ContactController;