/**
 * 作者：swl
 * product产品相关业务
 */
const contactModel = require('../models/contact.js');
const serverModel = require('../models/server.js');
const flowModel = require('../models/flow.js');
const about_usModel = require('../models/about_us.js');

const prodCateModel = require('../models/prodCate')
const productModel = require('../models/prod')
const moment = require('moment')
const mongoose = require('mongoose')
const url = require('url')


class ProductController {
    // 渲染产品列表前台页
    static async prodlist(req, res) {
            const links = {
                '1': '标志设计',
                '2': '品牌设计',
                '3': '画册包装'
            }
            const cateName = links[req.params.id]
            const { _id } = await prodCateModel.findOne({ cateName })
            const infos = await productModel.find({ cateId: _id, isLock: 1 })

            const serverinfo = await serverModel.find();
            const flowinfo = await flowModel.find();
            const about_usinfo = await about_usModel.find();
            const info = await contactModel.findOne();


            res.render('front/product-list', { title: cateName, infos, info: info, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo })
        }
        // 渲染产品详情前台页
    static async proddetail(req, res) {
            productModel.findById(req.params.id, async(error, doc) => {
                if (error) {
                    return res.render('error', { errorRs: ['页面走丢了！'], url: '/', time: 3 })
                }
                doc.viewCount = doc.viewCount + 1


                const serverinfo = await serverModel.find();
                const flowinfo = await flowModel.find();
                const about_usinfo = await about_usModel.find();
                const info = await contactModel.findOne();



                await doc.save()
                const { cateName } = await prodCateModel.findById(doc.cateId)
                res.render('front/product-detail', { info: doc, cateName, moment, infos: info, serverinfo: serverinfo, flowinfo: flowinfo, about_usinfo: about_usinfo })

            })
        }
        // 渲染产品分类列表后台页
    static async categoryList(req, res) {
            const infos = await prodCateModel.find()
            res.render('product/prod-cate-list', { title: '产品列表', infos })
        }
        // 渲染添加产品页面
    static async productAdd(req, res) {
            const categoryInfo = await prodCateModel.find()
            res.render('product/prod-add', { title: '产品添加', categoryInfo })
        }
        // 渲染产品的编辑页
    static async prodEdit(req, res) {
            const categoryInfo = await prodCateModel.find()
            const info = await productModel.findById(req.params.id)
            res.render('product/prod-edt', { title: '产品编辑', info, categoryInfo })
        }
        // 渲染产品列表后台页
    static async productList(req, res) {
            const category = url.parse(req.url, true).query.category
            let infos = await productModel.aggregate([{
                    $lookup: {
                        from: "prod_cate",
                        localField: "cateId",
                        /*关联表的id*/
                        foreignField: "_id",
                        as: "cateInfo"
                    }
                },
                { $unwind: "$cateInfo" }
            ])
            category && (infos = infos.filter(item => item.cateInfo.cateName === category))
            res.render('product/prod-list', { title: '产品详情', infos, moment })
        }
        /* TODO: 后台操作数据 */
        // 添加产品分类
    static async cateStore(req, res) {
            // 收集数据，然后定义表的模型，入库
            const { cateName } = req.body
            const cateObj = new prodCateModel({ cateName })
                // 校验错误
            const error = cateObj.validateSync()
            if (error) {
                res.render('error', {
                    errorRs: Object.keys(error.errors).map(attr => `${prodCateModel.fields[attr]} 字段：${error.errors[attr]}`),
                    url: '/admin/prodcate/add',
                    time: 3
                })
                return
            }
            // 信息合法
            try {
                await cateObj.save()
                res.redirect('/admin/prodcate/list')
            } catch (e) {
                res.redirect('/admin/prodcate/add')
            }
        }
        // 删除产品分类
    static async cateDelete(req, res) {
            const { id } = req.params
            await prodCateModel.deleteOne({ _id: id })
            res.redirect('back')
        }
        // 修改产品分类
    static cateEdit(req, res) {
            const { cateName, _id } = req.body
            prodCateModel.findById(_id, async(error, doc) => {
                if (error) {
                    res.redirect('back')
                } else {
                    doc.cateName = cateName
                    await doc.save()
                    res.redirect('/admin/prodcate/list')
                }
            })
        }
        // 添加产品
    static async productStore(req, res) {
            const { cateId, isLock, title, keywords, author, viewCount, markup, description } = req.body
            const cover = req.files['cover'] ? req.files['cover'][0].filename : ''
            const figure = req.files['figure'] ? req.files['figure'].map(file => file.filename) : null

            const newObj = new productModel({ cateId, isLock, title, keywords, author, viewCount, markup, description, cover, figure })
            const error = newObj.validateSync()
            if (error) {
                res.render('error', {
                    errorRs: Object.keys(error.errors).map(attr => `${productModel.fields[attr]} 字段：${error.errors[attr]}`),
                    url: '/admin/product/add',
                    time: 3
                })
                return
            }
            try {
                await newObj.save()
                res.redirect('/admin/product/list')
            } catch (e) {
                res.redirect('/admin/product/add')
            }
        }
        // 删除产品
    static async productDelete(req, res) {
            const { id } = req.params
            await productModel.deleteOne({ _id: id })
            res.redirect('back')
        }
        // 修改产品
    static async productEdit(req, res) {
            const { cateId, _id, isLock, title, keywords, author, viewCount, markup, description } = req.body
            const cover = req.files['cover'] ? req.files['cover'][0].filename : ''
            const figure = req.files['figure'] ? req.files['figure'].map(file => file.filename) : null

            productModel.findById(_id, async(error, doc) => {
                if (error) {
                    res.redirect('back');
                } else {
                    doc.cateId = cateId
                    doc.isLock = isLock
                    doc.title = title
                    doc.keywords = keywords
                    doc.author = author
                    doc.viewCount = viewCount
                    doc.markup = markup
                    doc.description = description
                    cover && (doc.cover = cover)
                    figure && (doc.figure = figure)
                    await doc.save()
                    res.redirect('/admin/product/list')
                }
            })
        }
        // 产品详情
    static async productDetail(req, res) {
            const [info] = await productModel.aggregate([
                { $match: { _id: mongoose.Types.ObjectId(req.params.id) } },
                {
                    $lookup: {
                        from: "prod_cate",
                        localField: "cateId",
                        /*关联表的id*/
                        foreignField: "_id",
                        as: "cateInfo"
                    }
                },
                { $unwind: "$cateInfo" }
            ])
            res.render('product/prod-detail', { info, moment })
        }
        // 控制产品是否显示
    static async productLock(req, res) {
        const { id } = req.params
        productModel.findById(id, async(error, doc) => {
            if (!error) {
                doc.isLock = doc.isLock ? 0 : 1
                await doc.save()
            }
            res.redirect('back')
        })
    }

    static aboutURI(req, res) {
        res.render('front/about-1')
    }
}

module.exports = ProductController