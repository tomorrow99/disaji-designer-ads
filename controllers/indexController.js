/* controller 翻译过来：控制器，主要是负责业务代码。为了后期的维护和新功能的增加，不建议把 routes下的文件写的过于臃肿。所以减负 */
/* 文件：业务名称Controller.js */
/* 这里写在 routers/news.js里面的路由对应的业务代码 */

/**
 * author：瞿倩婷
 * 功能：判断登录的cookie是否正确
 */

/* 类名：文件名的大驼峰 */

class IndexController {
    static InnerAdminIndex(req, res) {
        // res.json(req.cookies)
        if (req.cookies['isLogin'] != 1) {
            // 未登录  则继续在登录页面
            res.redirect('/admin/login')
        } else {
            // 登录过，则进入后台主页
            res.render('index', { username: req.cookies['username'] })
        }

    }
    static Welcome(req, res) {
        // res.send(formatDate())
        var nowTime = formatDate()
        res.render('welcome', { username: req.cookies['username'], nowTime });
    }

}

//时间戳转换方法    date:时间戳数字
function formatDate() {
    var date = new Date();
    var YY = date.getFullYear() + '-';
    var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
    // var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    // var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    // var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    // + " " + hh + mm + ss;
    return YY + MM + DD
}



module.exports = IndexController