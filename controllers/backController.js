/* controller 翻译过来：控制器，主要是负责业务代码。为了后期的维护和新功能的增加，不建议把 routes下的文件写的过于臃肿。所以减负 */
/* 文件：业务名称Controller.js */
/* 这里写我们在 routers/news.js里面的路由对应的业务代码 */

/* 类名：文件名的大驼峰 */
class BackController {

    static logout(req, res) {
        res.cookie('isLogin', 0);
        res.cookie('username', '');
        res.redirect('/admin/login');
    }
}

module.exports = BackController;


