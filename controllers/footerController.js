/**
* 作者:yzh
* 联系我们的业务功能
*/
const url = require("url");

const serverModel = require('../models/server.js');
const flowModel = require('../models/flow.js');
const about_usModel = require('../models/about_us.js');


class ServerController {
    static addServer = async (req, res) => {
        let { server_sort } = req.body;
        let cateObj = new serverModel({ server_sort });
        let error = cateObj.validateSync();
        if (error) {
            const errorRs = [];
            for (let attr in error.errors) {
                errorRs.push(`${serverModel.fields[attr]} 字段：${error.errors[attr]}`);
            }
            let url = '/admin/server/server_add';
            let time = 3;
            res.render('error', { errorRs, url, time });
            return;
        }

        // 信息合法
        try {
            let info = await cateObj.save();
            res.redirect('/admin/server/server-list');
        } catch (e) {
            res.redirect('/admin/server/server-list');
        }

    };
    static editServer = async (req, res) => {
        let cateId = req.params.id;
        /* 在编辑之前，应该先展示编辑内容的表单 */
        let info = await serverModel.findOne({ _id: cateId });
        res.render('footer/server-edt', { title: '服务的分类编辑', info });
    }
    static edit = async (req, res) => {
        let { server_sort, _id } = req.body;

        /* 根据当前的id 去查找记录然后在更新*/
        serverModel.findById(_id, async (error, doc) => {
            if (error) {
                res.redirect('back');
            } else {
                doc.server_sort = server_sort;
                await doc.save();
                res.redirect('/admin/server/server-list');

            }
        })
    }
    static delServer = async (req, res) => {
        /* 删除：删除的时候，我们知道删除哪一个分类信息，则我们在点击分类按钮的时候，需要传递一个分类的 _id */
        /*1. 直接通过url地址规则定义，一般我们称之为 路由参数  2. 通过 查询字符串传递*/
        console.log(url.parse(req.url, true));
        // res.json(url.parse(req.url, true));
        let cateId = url.parse(req.url, true).query.id;

        try {
            await serverModel.deleteOne({ _id: cateId });

            res.redirect('back'); // back 代表的是当前的页面的上一级页面。指的是这个的来源路径

        } catch (e) {
            res.redirect('back');

        }
    }
    static showServer = async (req, res) => {
        const infos = await serverModel.find();
        console.log(infos);
        res.render('footer/server-list', { title: '服务的分类展示', infos });
    }
    static server_add = async (req, res) => {
        //  const info = await serverModel.findOne();
        // if (info) {
        //     res.render('footer/server-add', {info}); // {}

        // } else {
        res.render('footer/server-add', { info: {} });
        // }
    }

};


class FlowController {
    static addFlow = async (req, res) => {
        let { flow_sort } = req.body;
        let cateObj = new flowModel({ flow_sort });
        let error = cateObj.validateSync();
        if (error) {
            const errorRs = [];
            for (let attr in error.errors) {
                errorRs.push(`${flowModel.fields[attr]} 字段：${error.errors[attr]}`);
            }
            let url = '/admin/flow/flow_add';
            let time = 3;
            res.render('error', { errorRs, url, time });
            return;
        }

        // 信息合法
        try {
            let info = await cateObj.save();
            res.redirect('/admin/flow/flow-list');
        } catch (e) {
            res.redirect('/admin/flow/flow-list');
        }

    };
    static editFlow = async (req, res) => {
        let cateId = req.params.id;
        /* 在编辑之前，应该先展示编辑内容的表单 */
        let info = await flowModel.findOne({ _id: cateId });
        res.render('footer/flow-edt', { title: '流程的分类编辑', info });
    }
    static edit = async (req, res) => {
        let { flow_sort, _id } = req.body;

        /* 根据当前的id 去查找记录然后在更新*/
        flowModel.findById(_id, async (error, doc) => {
            if (error) {
                res.redirect('back');
            } else {
                doc.flow_sort = flow_sort;
                await doc.save();
                res.redirect('/admin/flow/flow-list');

            }
        })
    }
    static delFlow = async (req, res) => {
        /* 删除：删除的时候，我们知道删除哪一个分类信息，则我们在点击分类按钮的时候，需要传递一个分类的 _id */
        /*1. 直接通过url地址规则定义，一般我们称之为 路由参数  2. 通过 查询字符串传递*/
        console.log(url.parse(req.url, true));
        // res.json(url.parse(req.url, true));
        let cateId = url.parse(req.url, true).query.id;

        try {
            await flowModel.deleteOne({ _id: cateId });

            res.redirect('back'); // back 代表的是当前的页面的上一级页面。指的是这个的来源路径

        } catch (e) {
            res.redirect('back');

        }
    }
    static showFlow = async (req, res) => {
        const infos = await flowModel.find();
        console.log(infos);
        res.render('footer/flow-list', { title: '流程的分类展示', infos });
    }
    static flow_add = async (req, res) => {

        res.render('footer/flow-add', { info: {} });

    }

};



class About_usController {
    static addAbout_us = async (req, res) => {
        let { about_s } = req.body;
        let cateObj = new about_usModel({ about_s });
        let error = cateObj.validateSync();
        if (error) {
            const errorRs = [];
            for (let attr in error.errors) {
                errorRs.push(`${about_usModel.fields[attr]} 字段：${error.errors[attr]}`);
            }
            let url = '/admin/about_us/about_us-add';
            let time = 3;
            res.render('error', { errorRs, url, time });
            return;
        }

        // 信息合法
        try {
            let info = await cateObj.save();
            res.redirect('/admin/about_us/about_us-list');
        } catch (e) {
            res.redirect('/admin/about_us/about_us-list');
        }

    };
    static editAbout_us = async (req, res) => {
        let cateId = req.params.id;
        /* 在编辑之前，应该先展示编辑内容的表单 */
        let info = await about_usModel.findOne({ _id: cateId });
        res.render('footer/about_us-edt', { title: '编辑关于我们', info });
    }
    static edit = async (req, res) => {
        let { about_s, _id } = req.body;

        /* 根据当前的id 去查找记录然后在更新*/
        about_usModel.findById(_id, async (error, doc) => {
            if (error) {
                res.redirect('back');
            } else {
                doc.about_s = about_s;
                await doc.save();
                res.redirect('/admin/about_us/about_us-list');

            }
        })
    }
    static delAbout_us = async (req, res) => {
        /* 删除：删除的时候，我们知道删除哪一个分类信息，则我们在点击分类按钮的时候，需要传递一个分类的 _id */
        /*1. 直接通过url地址规则定义，一般我们称之为 路由参数  2. 通过 查询字符串传递*/
        console.log(url.parse(req.url, true));
        // res.json(url.parse(req.url, true));
        let cateId = url.parse(req.url, true).query.id;

        try {
            await about_usModel.deleteOne({ _id: cateId });

            res.redirect('back'); // back 代表的是当前的页面的上一级页面。指的是这个的来源路径

        } catch (e) {
            res.redirect('back');

        }
    }
    static showAbout_us = async (req, res) => {
        const infos = await about_usModel.find();
        console.log(infos);
        res.render('footer/about_us-list', { title: '展示关于我们', infos });
    }
    static about_us_add = async (req, res) => {

        res.render('footer/about_us-add', { info: {} });

    }

};



// module.exports = About_usController;
module.exports = {
    ServerController,
    FlowController,
    About_usController
}
