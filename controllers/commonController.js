/* controller 翻译过来：控制器，主要是负责业务代码。为了后期的维护和新功能的增加，不建议把 routes下的文件写的过于臃肿。所以减负 */
/* 文件：业务名称Controller.js */
/* 这里写我们在 routers/news.js里面的路由对应的业务代码 */
const loginModel = require('../models/loginAdmin');
const md5 = require('md5');

/**
 * author：瞿倩婷
 * 功能：业务层，登录检查，验证密码，以及设置cookie
 */

/* 类名：文件名的大驼峰 */
class CommonController {
    static AdminLogin(req, res) {
        res.render('login', { title: '后台登录' });
    }
    static async LoginCheck(req, res) {
        // 接收用户提交的用户名和密码
        let { username, password } = req.body
            // //用户身份信息的校验 
        let userInfo = await loginModel.findOne({ username })
        if (userInfo) {
            // 存在
            if (userInfo.password == md5(password)) {
                // 密码正确
                // 记录登录的标识，实现跨请求数据共享
                res.cookie('isLogin', 1)
                res.cookie('username', username) //给浏览器发送cookie指令
                    //前往后台首页
                res.redirect('/admin/index')

            } else {
                // 密码不正确
                res.redirect('back')
            }
        } else {
            //不存在
            res.redirect('back')
        }

    }
}
module.exports = CommonController;